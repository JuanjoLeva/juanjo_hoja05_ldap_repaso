##Hoja05_LDAP_Repaso

###Ejercicio 2

###3

Crear en LDAP una nueva organizationalUnit llamado alumnos, con 6 usuarios, dos grupos y repartir tres usuarios por grupo

![imagen](imagenes/Captura1.PNG "1")

![imagen](imagenes/Captura2.PNG "2")

![imagen](imagenes/Captura3.PNG "3")

![imagen](imagenes/Captura4.PNG "4")

![imagen](imagenes/Captura4.1.PNG "4.1")
***
###4

Crear un proyecto dinámico en eclipse y añadir un fichero index.jsp

![imagen](imagenes/Captura5.PNG "5")

![imagen](imagenes/Captura6.PNG "6")

![imagen](imagenes/Captura7.PNG "7")

![imagen](imagenes/Captura8.PNG "8")

***
###5

Configurar la aplicación repaso con JNDIRealm para que solo puedan entrar los alumnos de daw2

![imagen](imagenes/Captura9.PNG "9")

![imagen](imagenes/Captura18.PNG "18")

![imagen](imagenes/Captura11.PNG "11")

***
###6

Desplegar la aplicación usando ant

Voy a Windows->Preferences->Ant->Runtime

![imagen](imagenes/Captura12.PNG "12")

Añado estas librerías que se encuentran en usr/share/tomcat9/lib y usr/share/tomcat9/bin

![imagen](imagenes/Captura13.PNG "13")

![imagen](imagenes/Captura14.PNG "14")

Habilito la vista Ant

![imagen](imagenes/Captura15.PNG "15")

Creo el build.xml

![imagen](imagenes/Captura16.PNG "16")

![imagen](imagenes/Captura17.PNG "17")

Creo el .war y despliego la aplicacion

![imagen](imagenes/Captura22.PNG "22")

![imagen](imagenes/Captura20.PNG "20")

![imagen](imagenes/Captura21.PNG "21")

![imagen](imagenes/Captura23.PNG "23")

![imagen](imagenes/Captura24.PNG "24")

El login funcionaba bien pero al entrar no me mostraba la hora, me mostraba un error que no he sabido solucionar. Si fallaba al poner el usuario o contraseña sí que me saltaba bien el error-login.

***
###Ejercicio 3

###7

Crear un host virtual llamado repaso.com y desplegar ahi la aplicación

Edito el fichero hosts que está en /etc

![imagen](imagenes/Captura25.PNG "25")

Creo en /var/lib/tomcat9 un directorio llamado webapps-repaso.

![imagen](imagenes/Captura26.PNG "26")

Le otorgo permisos

![imagen](imagenes/Captura27.PNG "27")

Edito el fichero server.xml en /var/lib/tomcat9/conf

![imagen](imagenes/Captura28.PNG "28")

Reinicio y veo que se ha creado el fichero

![imagen](imagenes/Captura29.PNG "29")

copio manager.xml de localhost a repaso.com

![imagen](imagenes/Captura30.PNG "30")

![imagen](imagenes/Captura31.PNG "31")

***
###8

Hacer que la aplicación solo funcione por SSL

Como ya generé en otra práctica la keytool he utilizado esa

![imagen](imagenes/Captura34.png "34")

Edito server.xml en /var/lib/tomcat9/conf para configurar un conector

![imagen](imagenes/Captura32.PNG "32")

Veo que el puerto 8443 está escuchando

![imagen](imagenes/Captura35.png "35")

La aplicación está desplegada en el manager del host virtual

![imagen](imagenes/Captura33.PNG "33")